var container
var scene, camera, renderer, controls
var Y_AXIS, camera_pivot

var mouseX = 0, mouseY = 0;
var start_time = Date.now();

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

init();
animate();


function init(){
    // setup scene
    scene = new THREE.Scene();

    // setup camera
    camera = new
    THREE.PerspectiveCamera(
        55,
        window.innerWidth/window.innerHeight,
        45,
        30000
    );

    //setup lights
    scene.add(new THREE.AmbientLight( 0x666666 ) );

    //load skybox
    // made skybox with Spacescape http://alexcpeterson.com/spacescape/
    var textureLoader = new THREE.TextureLoader();

    let materialArray = [];
    let texture_ft = textureLoader.load("./assets/brain_space_front.png");
    let texture_bk = textureLoader.load("./assets/brain_space_back.png");
    let texture_up = textureLoader.load("./assets/brain_space_up.png");
    let texture_dn = textureLoader.load("./assets/brain_space_down.png");
    let texture_rt = textureLoader.load("./assets/brain_space_right.png");
    let texture_lf = textureLoader.load("./assets/brain_space_left.png");
    
    materialArray.push(new THREE.MeshBasicMaterial( { map: texture_ft }));
    materialArray.push(new THREE.MeshBasicMaterial( { map: texture_bk }));
    materialArray.push(new THREE.MeshBasicMaterial( { map: texture_up }));
    materialArray.push(new THREE.MeshBasicMaterial( { map: texture_dn }));
    materialArray.push(new THREE.MeshBasicMaterial( { map: texture_rt }));
    materialArray.push(new THREE.MeshBasicMaterial( { map: texture_lf }));

    for (let i = 0; i < 6; i++) {
        materialArray[i].side = THREE.BackSide;
    }
    let skyboxGeo = new THREE.BoxGeometry( 10000, 10000, 10000);
    let skybox = new THREE.Mesh( skyboxGeo, materialArray );
    scene.add( skybox );

    // pivot camera
    camera_pivot = new THREE.Object3D()
    Y_AXIS = new THREE.Vector3( 0, 1, 0 );
    scene.add( camera_pivot );
    camera_pivot.add( camera );
    camera.position.set( 500, 0, 0 );
    camera.lookAt( camera_pivot.position );

    // renderer
    var canvas = document.querySelector("canvas");
    renderer = new THREE.WebGLRenderer();
    document.getElementById("canvas").appendChild(renderer.domElement);
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.setClearColor(0xF0F0F0);

    // controls
    //var controls = new THREE.OrbitControls(camera, renderer.domElement);


    window.addEventListener( 'resize', onWindowResize, false );
    
}

function animate() {

    camera_pivot.rotateOnAxis( Y_AXIS, 0.001 );    // radians

    renderer.render(scene, camera);
    requestAnimationFrame(animate);
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
}
